# Makefile for ement-extras Elisp package

# Author: Dima Akater

ifndef SITELISP
export SITELISP = /usr/share/emacs/site-lisp
else
export SITELISP
endif

ifndef LISPDIR
export LISPDIR = ${SITELISP}/ement-extras
else
export LISPDIR
endif


UMAKEDEPS = org
UMAKEDEPS:=$(UMAKEDEPS:%=-L $(SITELISP)/%)

BUILDDEPS = defmacro-gensym ement plz sgc
BUILDDEPS:=$(BUILDDEPS:%=-L $(SITELISP)/%)

EMACS_INIT 	:= emacs -q --batch -L umake --load init.el
# should be
# EMACS_INIT 	:= emacs -Q --batch -L umake --load init.el
# but most people don't have a proper SITELISP

EMACS_UMAKE 	:= $(EMACS_INIT) $(UMAKEDEPS) --load umake.el

EVAL		:= $(EMACS_UMAKE) $(BUILDDEPS) -L . --eval

.PHONY: default all install clean uninstall

default:
	$(EVAL) "(umake 'default)"

all:
	$(EVAL) "(umake 'all)"

install:
	$(EMACS_INIT) --load install.el

clean:
	rm -rf *.el{,c} site-gentoo.d
# umake 'clean would work too but this is faster,
# and clean is run often

uninstall:
	rm -vf ${LISPDIR}/*.el
	rm -vf ${LISPDIR}/*.elc
	rm -r ${LISPDIR}
# todo: umake 'uninstall
