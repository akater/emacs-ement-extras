# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: ement-auth-source
#+description: auth-source support for Ement, Matrix client for Emacs
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle ement-auth-source.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) BUG(b@/!) | DONE(d@)
#+todo: TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Overview

* Meta
** Metadata
#+begin_src emacs-lisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: matrix, messaging

#+end_src

** Dependencies
#+begin_src emacs-lisp :results none
(eval-when-compile (require 'subr-x))
(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'rx))
(eval-when-compile (require 'sgc))
(require 'sgc)
(require 'auth-source)
(require 'nadvice)
(with-no-messages                       ; available at compile time in umake
  ;; do not notify on lack of ImageMagick support when compiling
  (require 'ement))
(require 'ement-extras-core)
#+end_src

* Group ement-auth-source
#+name: defgroup ement-auth-source
#+begin_src emacs-lisp :results none
(defgroup ement-auth-source nil
  "Settings for using auth-source with Ement."
  :prefix "ement-auth-source-"
  :group 'ement)
#+end_src

* Variables
** Being verbose
#+name: defcustom ement-auth-source-verbose
#+begin_src emacs-lisp :results none
(defcustom ement-auth-source-verbose nil
  "Complain when passwords are searched but can't be found."
  :type 'boolean
  :group 'ement-auth-source)
#+end_src

** Being enabled
#+name: defvar ement-auth-source
#+begin_src emacs-lisp :results none
(defvar ement-auth-source nil)
#+end_src

* defstumbethod
** auth-source user-id action
#+begin_src emacs-lisp :results none
(sgc-defstubmethod auth-source (user-id ement-connection-properties) :action
  (when ement-auth-source
    ;; sgc probably should have built-in mechanism for such conditions
    (let ((user-id (known user-id)))
      (ement-extras-parse-user-id user-id)
      (know user (match-string 1 user-id))
      (know host (match-string 2 user-id)))))
#+end_src

** auth-source password heuristic
#+begin_src emacs-lisp :results none
(sgc-defstubmethod auth-source (password ement-connection-properties) :heuristic
  (when ement-auth-source
    (if-let* ((user (known user))
              (host (known host))
              (auth-source-secret
               (plist-get
                (car (auth-source-search :max 1 :user user :host host))
                :secret)))
        (know (if (functionp auth-source-secret)
                  (funcall auth-source-secret)
                auth-source-secret))
      (when ement-auth-source-verbose
        (message "Could not find password for %s with auth-source"
                 (known user-id))))))
#+end_src

* Enabling
#+name: defun ement-auth-source-enable
#+begin_src emacs-lisp :results none
;;;###autoload
(defun ement-auth-source-enable ()
  (interactive)
  (setq ement-auth-source t))
#+end_src

#+name: defun ement-auth-source-disable
#+begin_src emacs-lisp :results none
;;;###autoload
(defun ement-auth-source-disable ()
  (interactive)
  (setq ement-auth-source nil))
#+end_src

* Alternative definitions
This is somewhat cleaner but we decided against it because ~cl-generic~ explicitly recommends against too much method redefinitions.
#+begin_src emacs-lisp :tangle no :results none
;;;###autoload
(defun ement-auth-source-enable ()
  (interactive)
  (sgc-defstubmethod auth-source (user-id ement-connection-properties) :action
    (cl-destructuring-bind (user host) (split-string (known user-id) "@")
      (know user user)
      (know host host)))
  (sgc-defstubmethod auth-source (password ement-connection-properties) :heuristic
    (if-let* ((user (known user))
              (host (known host))
              (auth-source-secret
               (plist-get
                (car (auth-source-search :max 1 :user user :host host))
                :secret)))
        (know (if (functionp auth-source-secret)
                  (funcall auth-source-secret)
                auth-source-secret))
      (when ement-auth-source-verbose
        (message "Could not find password for %s with auth-source"
                 (known user-id))))))
#+end_src

#+begin_src emacs-lisp :tangle no :results none
;;;###autoload
(defun ement-auth-source-disable ()
  (interactive)
  (setf (sgc-heuristics 'password 'ement-connection-properties)
        (cl-delete 'auth-source
                   (sgc-heuristics 'password 'ement-connection-properties)
                   :key #'car
                   :test #'eq)
        (sgc-actions 'password 'ement-connection-properties)
        (cl-delete 'auth-source
                   (sgc-actions 'password 'ement-connection-properties)
                   :key #'car
                   :test #'eq)))
#+end_src
