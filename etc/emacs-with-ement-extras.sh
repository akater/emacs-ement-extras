#!/bin/sh

# Safe environment ready for ement-connect.
# For testing purposes.

SITELISP=${SITELISP=/usr/share/emacs/site-lisp}

emacs -Q                                  \
      -L ${SITELISP}/dash                 \
      -L ${SITELISP}/ement                \
      -L ${SITELISP}/ement-extras         \
      -L ${SITELISP}/plz                  \
      -L ${SITELISP}/s                    \
      -L ${SITELISP}/ts                   \
      --eval "(require 'ement)"           \
      --eval "(require 'ement-room-list)" \
      --eval "                            \
(setq ement-room-list-avatars nil         \
      ement-room-avatars nil              \
      ement-room-images nil)"

