# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: ement-extras-core
#+description: Essential functions for ement-extras Elisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle ement-extras-core.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) BUG(b@/!) | DONE(d@)
#+todo: TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Overview

* Meta
** Metadata
#+begin_src emacs-lisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: matrix, messaging

#+end_src

** Compatibility
:PROPERTIES:
:header-args:    :tangle (if (>= emacs-major-version 28) "ement-extras-core.el" "")
:END:
This is temporarily, until we native-compile with umake correctly.  We don't prefix because of that and because Emacs 28 is not released anyway.

#+begin_src emacs-lisp :results none
(eval-when-compile (require 'cl-macs))
(defmacro with-no-messages (&rest body)
  (declare (indent 0))
  `(progn
     (advice-add 'message :around #'ignore)
     (unwind-protect (cl-locally ,@body)
       (advice-remove 'message #'ignore))))
#+end_src

** Dependencies
#+begin_src emacs-lisp :results none
(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'rx))
(require 'sgc)
(with-no-messages                       ; available at compile time in umake
  ;; do not notify on lack of ImageMagick support when compiling
  (require 'ement))
(require 'ement-extras-utils)
#+end_src

* Variables
** user-id-history
#+name: defvar ement-extras-user-id-history
#+begin_src emacs-lisp :results none
(defvar ement-extras-user-id-history nil)
#+end_src

** keep-user-id-history
#+name: defcustom ement-extras-keep-user-id-history
#+begin_src emacs-lisp :results none
(defcustom ement-extras-keep-user-id-history nil
  "When set to a variable, keep in it user-ids submitted to ement-connect."
  :type '(choice (const nil)
                 (variable-item ement-extras-user-id-history)
                 variable)
  :group 'ement)
#+end_src

* parse-user-id
#+begin_src emacs-lisp :results none
(defun ement-extras-parse-user-id (user-id)
  (string-match
   (rx bos
       "@" (group (1+ (not (any ":"))))               ; Username
       ":" (group (optional (1+ (not (any blank)))))) ; Server name
   user-id))
#+end_src

* connect
** Prerequisites
*** Variables to pass properties to proxy buffer
#+begin_src emacs-lisp :results none
(defvar-local ement-connect-parameters nil)
#+end_src
We will likely pass stub object instead.

** Definition
#+name: defun ement-extras--connect
#+begin_src emacs-lisp :results none
(defun ement-extras--connect (ement-connect &rest connection-properties)
  "A wrapper for `ement-connect' implementing completion-based interface."
  (interactive)
  (condition-case err (apply ement-connect
                             (apply #'sgc-make
                                    'sgc--plist 'ement-connection-properties
                                    connection-properties))
    ;; I wonder if I'll have to implement restarts as well
    (sgc-incomplete-stub
     (cl-destructuring-bind (_ stub handler) err
       (if (functionp handler)
           (funcall handler stub)
         (apply #'signal err))))))
#+end_src

* Advising
#+name: defun ement-extras-enable
#+begin_src emacs-lisp :results none
;;;###autoload
(defun ement-extras-enable ()
  (interactive)
  (advice-add 'ement-connect :around #'ement-extras--connect))
#+end_src

~ement-extras-enable~ actually happens unconditionally on loading ~ement-extras~.

#+name: defun ement-extras-disable
#+begin_src emacs-lisp :results none
;;;###autoload
(defun ement-extras-disable ()
  (interactive)
  (advice-remove 'ement-connect #'ement-extras--connect))
#+end_src
* connection-properties
#+begin_src emacs-lisp :results none
;; Very weird bug with provide
(provide 'ement-extras-core)
(sgc-defstub ement-connection-properties ()
  (session)
  ;; todo: can we do something meaningful with pantalaimon sessions?
  (uri-prefix
   ;; We don't specify type string because it could be 'pantalaimon
   )
  (user-id
   :type string
   :default
   (cl-loop
    as user-id = (read-string "User ID: "
                              nil ement-extras-keep-user-id-history)
    if (not (ement-extras-parse-user-id user-id))
    ;; todo: know user and host right here
    do (minibuffer-message "Invalid user ID format: use @USERNAME:SERVER")
    else
    return user-id))
  (password :type string
            :default
            ;; check: used to be “unless (plist-get stub :proxy)”
            (read-passwd "Password: ")))
#+end_src

