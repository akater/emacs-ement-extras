;; -*- lexical-binding: t -*-

(with-no-messages (load "umake-def"))

(defconst sitelisp
  (file-name-as-directory
   (or (getenv "SITELISP")
       (let ((default-sitelisp "/usr/share/emacs/site-lisp/"))
         (warn "SITELISP is not set. Defaulting to %s" default-sitelisp)
         default-sitelisp))))

(defvar use-flags (read-list (getenv "USE")))

(defvar site-autoloads (file-directory-p+
                        (expand-file-name "site-gentoo.d" sitelisp)))

(defvar autoloads-file (format "%s-autoloads.el" umake-feature))

(defconst lispdir (file-name-as-directory
                   (or (getenv "LISPDIR")
                       (expand-file-name (concat (ensure-string umake-feature)
                                                 ;; because this is
                                                 ;; how it's done in Portage
                                                 "/")
                                         sitelisp))))

(defconst org-immutable-sources-directory
  (getenv "ORG_IMMUTABLE_SOURCES_DIRECTORY"))

(defconst org-local-sources-directory (getenv "ORG_LOCAL_SOURCES_DIRECTORY"))
;; with akater-sh-env, it would be
;; (defconstenv org-local-sources-directory)
