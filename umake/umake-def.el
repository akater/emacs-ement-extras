;; -*- lexical-binding: t -*-

(defconst-with-prefix umake
  feature 'ement-extras
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  source-files-in-order '( "ement-extras-utils"
                           "ement-extras-core"
                           "ement-auth-source"
                           "ement-pantalaimon" "ement-proxy"
                           "ement-extras")
  site-lisp-config-prefix "50")
